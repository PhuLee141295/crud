import { Injectable } from '@angular/core';
import { Product } from '../model/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor() { }

  products: Product[] = [{
    id: 1,
    name: 'IPhone 12',
    price: 2400000,
    description: 'New'
  }, {
    id: 2,
    name: 'IPhone 11',
    price: 1560000,
    description: 'Like new'
  }, {
    id: 3,
    name: 'IPhone X',
    price: 968000,
    description: '97%'
  }, {
    id: 4,
    name: 'IPhone 8',
    price: 7540000,
    description: '98%'
  }, {
    id: 5,
    name: 'IPhone 11 Pro',
    price: 1895000,
    description: 'Like new'
  }];

  getAll() {
    return this.products;
  }
  get(id: number): Product {
    let result: Product = {};
    for (var i = 0; i < this.products.length; i++) {
      if (this.products[i].id == id) {
        result = this.products[i];
        break;
      }
    }
    return result;
  }

  saveProduct(product: Product) {
    this.products.push(product);
  }

  updateProduct(id: number, product: Product) {
    for (var i = 0; i < this.products.length; i++) {
      if (this.products[i].id == id) {
        this.products[i] = product;
        this.products[i].id = id;
        break;
      }
    }
  }

  destroy(id: number) {
    for (var i = 0; i < this.products.length; i++) {
      if (this.products[i].id == id) {
        console.log(this.products[i]);
        this.products.splice(i, 1);
        break;
      }
    }
  }

  search(keyword: any) {

    let results: Product[] = [];
    for (var i = 0; i < this.products.length; i++) {
      // if (this.products.indexOf(keyword.search) ) {

      // }
      if (this.products[i].id == keyword || this.products[i].name == keyword || this.products[i].price == keyword) {
        let result: Product = {};
        result = this.products[i];
        results.push(result);
      }
    }

    // let filtered_users = this.products.filter(function (user: Product) {
    //   return user.indexOf(keyword) > -1;
    // });

    return results;
  }

}
