
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ProductService } from 'src/app/service/product.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-create',
  templateUrl: './product-create.component.html',

})
export class ProductCreateComponent implements OnInit {

  productForm !: FormGroup;

  submitted = false;

  constructor(private productService: ProductService, private route: Router) { }

  ngOnInit(): void {
    this.productForm = new FormGroup({
      "id": new FormControl('', [
        Validators.required
      ]),
      "name": new FormControl('', [
        Validators.required
      ]),
      "price": new FormControl('', [
        Validators.required
      ]),
      "description": new FormControl('', [
        Validators.required
      ]),
    });
  }

  submit() {
    this.submitted = true;

    if (!this.productForm.valid) {
      return;
    } else {
      const product = this.productForm.value;
      this.productService.saveProduct(product);
      this.route.navigate(['product/list']);
    }

  }

}
