import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Product } from 'src/app/model/product';
import { ProductService } from 'src/app/service/product.service';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',

})
export class ProductEditComponent implements OnInit {
  productForm !: FormGroup;
  product !: Product;
  submitted: boolean = false;
  id: number = this.activatedRouteService.snapshot.params['id']

  constructor(private productService: ProductService,
    private activatedRouteService: ActivatedRoute,
    private route: Router) { }

  ngOnInit(): void {
    this.get(this.id);
    //dùng khi có sự thay đổi trên url
    // this.activatedRouteService.params.subscribe(
    //   (params: Params) => {
    //     let id = parseInt(params['id']);
    //     this.get(id);
    //   }
    // );

    this.productForm = new FormGroup({
      "name": new FormControl(this.product.name, [
        Validators.required
      ]),
      "price": new FormControl(this.product.price, [
        Validators.required
      ]),
      "description": new FormControl(this.product.description, [
        Validators.required
      ]),
    });

  }

  get(id: number) {
    this.product = this.productService.get(id);
  }

  submit() {
    this.submitted = true;

    if (!this.productForm.valid) {
      return;
    } else {
      const product = this.productForm.value;
      this.productService.updateProduct(this.id, product);
      this.route.navigate(['product/list']);
    }

  }

}
