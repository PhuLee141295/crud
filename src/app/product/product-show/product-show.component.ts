import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Product } from 'src/app/model/product';
import { ProductService } from 'src/app/service/product.service';

@Component({
  selector: 'app-product-show',
  templateUrl: './product-show.component.html',
})
export class ProductShowComponent implements OnInit {

  product !: Product;
  id: number = this.activatedRouteService.snapshot.params['id']

  constructor( private productService: ProductService, private activatedRouteService: ActivatedRoute) { }

  ngOnInit(): void {
    this.get();
  }


  get(){
    this.product = this.productService.get(this.id);
  }
}
