import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Product } from 'src/app/model/product';
import { ProductService } from 'src/app/service/product.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
})

export class ProductListComponent implements OnInit {

  searchForm !: FormGroup;
  products: Product[] = [];

  constructor(private productService: ProductService) { }

  ngOnInit(): void {
    this.getAll();

    this.searchForm = new FormGroup({
      'search': new FormControl('', [])
    })
  }

  getAll(): void {
    this.products = this.productService.getAll();
  }

  search() {
    const keyword = this.searchForm.controls.search.value;
    this.products = this.productService.search(keyword)
  }
}



