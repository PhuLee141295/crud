import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from 'src/app/model/product';
import { ProductService } from 'src/app/service/product.service';

@Component({
  selector: 'app-product-destroy',
  templateUrl: './product-destroy.component.html',

})
export class ProductDestroyComponent implements OnInit {

  product !: Product;
  id: number = this.activatedRouteService.snapshot.params['id']

  constructor(private productService: ProductService, private activatedRouteService: ActivatedRoute, private route: Router) { }

  ngOnInit(): void {
    this.get();
  }
  get() {
    this.product = this.productService.get(this.id)
  }

  destroy(): void {
    this.productService.destroy(this.id);
    this.route.navigate(['product/list']);
  }

}
